$('#SortType').on('change', function(){
  reloadData();
});
$('#keyword').on('keyup', function(){
  reloadData();
});
$('#list').on('click', function(){
  $('#viewType').val('list');
  reloadData();
});
$('#grid').on('click', function(){
  $('#viewType').val('grid');
  reloadData();
});
$('#page0').on('click', function(){
  $('#pageNum').val(0);
  reloadData();
});
$('#page1').on('click', function(){
  $('#pageNum').val(1);
  reloadData();
});
$('#page2').on('click', function(){
  $('#pageNum').val(2);
  reloadData();
});


function reloadData(){
  var keyword = $('#keyword').val();
  var viewType = $('#viewType').val();
  var pageNum = $('#pageNum').val();
  var SortType = $('#SortType').val();
  $.ajax({
    url: "atiApi.php",
    method: 'POST',
    data: {
      keyword: keyword,
      viewType: viewType,
      pageNum: pageNum,
      SortType: SortType,
    }
  }).done(function(output){
    $("#products").empty();
    $("#products").append(output);

  });
}

$(function () {
  reloadData();
});