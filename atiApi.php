<?php

header('Content-Type: text/html; charset=utf-8');
global $page_num;
$page_num = $_POST['pageNum'];
global $SortType;
$SortType = $_POST['SortType'];
$json = file_get_contents("https://ed808.com:92/latin/intern?parameter[page]=$page_num&parameter[sort]=$SortType");
$products = json_decode($json);
$products = (array)$products->contents;
$keyword = $_POST['keyword'];
$viewType = $_POST['viewType'];
switch($viewType){
  case 'list':
    require_once('products/product-linear.php');
    break;
  case 'grid':
    require_once('products/product-grid.php');
    break;
  default:
     require_once('products/product-grid.php');
}
