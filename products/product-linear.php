<?php


foreach($products as $product) {
  $product = (array) $product;
  $id = $product['nid'];
  $name = $product['title'];
  $thumbnail = $product['picture'];
  $created_at = date("Y/m/d",$product['created']);

?>

  <div class="productPanel-linear">
    <div class="productThumbWrapper-linear">
      <img src="<?=$thumbnail?>" class="productThumb-linear" >
    </div>
    <div class="productPanelRightSide">
    <span class="productname-linear"><?=$name?></span>
  
    <span class="productname"><?=$created_at?></span>
   <div class="productBtnWrapper-linear">
    <span class="wishbtn-linear ic-star-full" style="margin-right: 10px;"></span>
    
    </span>
    </div>
    </div>
  </div>
<?php }?>
