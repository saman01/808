<html>

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="asset/style/base.css">
  <link rel="stylesheet" href="asset/style/color.css">
  <link rel="stylesheet" href="asset/style/elements.css">
  <link rel="stylesheet" href="asset/style/font.css">
  <link rel="stylesheet" href="asset/style/icon.min.css">
  <link rel="stylesheet" href="asset/style/theme.min.css">
</head>

<body>
  <div style="width: 93%;margin: 0 auto; text-align: center;">
    <div style="width: 100%;background-color: #dbdddb;padding: 5px;border-radius: 3px;">
    <strong class="m20r">صفحه</strong>
    <div class="dib" style="margin-top: 9px;">
        <span  id="page0"  style="font-size: 13pt;margin-right: 10px;cursor: pointer;">1</span>
        <span class="m15" id="page1"  style="font-size: 13pt;cursor: pointer;">2</span>
        <span  id="page2"  style="font-size: 13pt;cursor: pointer;">3</span>
        <input id="pageNum" type="hidden" value=0>
      </div>
      <strong style="margin-right: 20px;">ترتیب</strong>
      <select id="SortType" style="width: 140px;">
      <option value="first">از اول به آخر</option>
      <option value="last" selected>از آخر به اول</option>
      </select>

      <strong style="margin-right: 20px;">جستجو</strong>
      <input value="" placeholder="جستجو" id="keyword">

      <strong style="margin: 0 20px;">نوع نمایش</strong>
      <div class="dib" style="margin-top: 9px;">
        <span id="list" class="icon ic-list2" style="font-size: 11pt;margin-right: 10px;cursor: pointer;"></span>
        <span id="grid" class="icon ic-table2" style="font-size: 11pt;cursor: pointer;"></span>
        <input id="viewType" type="hidden" value="grid">
      </div>

    </div>
    <div id="products"></div>

  </div>

  <script src="asset/js/jquery-3.3.1.min.js"></script>
  <script src="asset/js/aja.js"></script>
</body>

</html>